
-- A
SELECT * FROM `artists` WHERE `name` LIKE '%d%';

-- B
SELECT * FROM `songs` WHERE `length` <= TIME('00:03:50');

-- C
SELECT  `album_title`, `song_name`, `length` 
FROM    `songs` INNER JOIN `albums` 
ON      `songs`.`album_id` = `albums`.`id`;

-- D
SELECT  *
FROM    `albums` INNER JOIN `artists` 
ON      `albums`.`artist_id` = `artists`.`id`
WHERE   `album_title` LIKE '%a%';

-- E
SELECT * FROM `albums` ORDER BY `album_title` DESC LIMIT 4;

-- F
SELECT  *
FROM    `albums` INNER JOIN `songs` 
ON      `songs`.`album_id` = `albums`.`id`
ORDER BY `album_title` DESC;