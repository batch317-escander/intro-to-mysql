-- 1
SELECT * FROM `customers` WHERE `country` ='Philippines'

-- 2
SELECT `contactLastName`, `contactFirstName` FROM `customers` WHERE `customerName`='La Rochelle Gifts'

-- 3
SELECT  productName, MSRP FROM products WHERE productName ='The Titanic'

-- 4
SELECT lastName, firstName FROM employees WHERE email ='jfirrelli@classicmodelcars.com';

-- 5
SELECT * FROM customers WHERE state IS NULL;

-- 6
SELECT firstName, lastName, email FROM employees WHERE lastName ='Patterson' AND firstName ='Steve';

-- 7
SELECT customerName, country, creditLimit FROM customers WHERE country <> 'USA' AND creditLimit > 3000;

-- 8
SELECT * FROM orders WHERE comments LIKE '%DHL%';

-- 9
SELECT * FROM productlines WHERE textDescription LIKE '%state of the art%';

-- 10
SELECT country FROM customers GROUP BY country HAVING COUNT(country) = 1;

-- 11
SELECT status FROM orders GROUP BY status HAVING COUNT(status) = 1;


-- 12
SELECT customerName FROM customers WHERE country IN ('USA', 'France', 'Canada');

-- 13
SELECT  firstName, lastName, city 
FROM    employees INNER JOIN offices 
ON      employees.officeCode = offices.officeCode 
WHERE   city ='Tokyo';

-- 14 
SELECT  customerName 
FROM    customers 
WHERE   salesRepEmployeeNumber = (
    SELECT  employeeNumber 
    FROM    employees 
    WHERE   firstName ='Leslie' 
    AND     lastName ='Thompson'
    );


-- 15
SELECT  	productName, customerName
FROM 		customers INNER JOIN orders
ON 			customers.customerNumber = orders.customerNumber
INNER JOIN 	orderdetails
ON			orders.orderNumber = orderdetails.orderNumber
INNER JOIN 	products
ON			orderdetails.productCode = products.productCode
WHERE 		customerName ='Baane Mini Imports'

-- 16
SELECT 	 	employees.firstName, employees.lastName, customerName, offices.country
FROM 		customers INNER JOIN employees
ON 			customers.salesRepEmployeeNumber = employees.employeeNumber
INNER JOIN 	offices
ON			employees.officeCode = offices.officeCode
WHERE		customers.city = offices.city 

-- 17
SELECT productName, quantityInStock FROM products WHERE productLine ='planes' AND quantityInStock < 1000

-- 18
SELECT customerName FROM customers WHERE phone LIKE '%+81%';